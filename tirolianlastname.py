#!/usr/bin/python
# coding: UTF-8, break: linux, indent: 4 spaces, lang: python/eng
"""
Generates random tyrolian sounding family names.

Applications include:
    - Remedy for mass amnesia
    - Batch naming of orphans and John Does
    - Election fraud

Website:
    https://bitbucket.org/jacgb/uibk-scripts/src/master/

Usage:
    %(progname)s --help | --version
    %(progname)s [options] [-v...]

Options:
    -n=<int>       Number of names to generate [default: 5]
    -v --verbose   Specify (multiply) to increase output
"""

#=======================================================================

from __future__ import division, print_function, unicode_literals
from logging import info, debug, error, warning as warn
from docopt import docopt
import os, logging, random as rnd

#=======================================================================

# ToDo get name elemens from: https://www.tirol.gv.at/kunst-kultur/landesarchiv/forschungstipps/familiennamen/
# and distinguis first from last by entry in German dictionary

part1 = set([
        'Trenk', 'Kimm', 'Eich', 'Kurz', 'Lang', 'Ober', 'Unter', 'Wald', 'Stein', 'Berg',
        'Licht', 'Wend', 'Reit', 'Ritt', 'Hitt', 'Licht', 'Leucht', 'Prand', 'Kirch', 'Gamp',
        'Gump', 'Eb', 'Vogel', 'Zill', 'Schwarz', 'Weiß', 'Rieg', 'Tanz', 'Wacht', 'Buch',
        'Eich', 'Sieb', 'Asch', 'Greif', 'Stock', 'Moos', 'Wink', 'Mau', 'Walch', 'Zemm',
        'Wies', 'Glatz', 'Frass', 'Telch', 'Resch', 'Pürg', 'Schlag', 'Treib', 'Kass', 'Lass',
        'Bach', 'Sax', 'Zwisch', 'Zirb', 'Zwaig', 'Ness', 'Thurn', 'Schatz', 'Schmatz', 'Katz',
        'Pell', 'Pflüg', 'Lieb', 'Sau', 'Leib', 'Wint', 'Somm', 'Wall', 'Schenk', 'Scherz',
        'Leb', 'Lind', 'Last', 'Schwein', 'Glanz', 'Schein', 'Stock', 'Stieg', 'Gott', 'Spar',
        'Fesch', 'Feuer', 'Mitt', 'Auf', 'Ab', 'Nieder', 'Tief', 'Hoch', 'Seicht', 'Axel', 'Axt',
        'Beil', 'Joch', 'Schwert', 'Kling', 'Breit', 'Groß', 'Los', 'Floß', 'Mast', 'Ast', 'Last',
        'Fass', 'Rad', 'Speich', 'Brnd', 'Rast', 'Herr', 'Frau', 'Dirn', 'Apf', 'Schlaf', 'Erb',
        'Tann', 'Bett', 'Öd', 'Scheid', 'Schied', 'Herz'
])
part2 = set([
        'thal', 'berg', 'kirch', 'bach', 'neid', 'haus', 'tuml', 'dorf', 'statt', 'egg',
        'nest', 'wald', 'bach', 'ung', 'stein', 'baum', 'stall', 'hof', 'hirt', 'mey',
        'egg', 'hub', 'walz', 'zieh', 'dreh', 'feld', 'stall', 'au', 'höf', 'nöff', 'steig',
        'furth', 'fress', 'heim', 'haupt', 'dreh', 'lob', 'löb', 'kirch', 'meist', 'wies'
        'tran', 'trög', 'trog', 'trag', 'dresch', 'resch', 'flös', 'schwert', 'träg', 'säg',
        'stätt', 'nass', 'märz', 'gaff', 'schaft', 'birn', 'schirm', 'schaff', 'auf', 'nied',
        'schmid', 'zug', 'zeug', 'pflanz', 'häus', 'näss', 'öd', 'richt', 'raum', 'platz'
])
noen = set([
        'es', 'er', 'el', 'en'
])

def do_stuff(args):
    for i in range(0,int(float(args['-n']))):
        first = rnd.sample(part1, 1)[0]
        en = rnd.sample(noen, 1)[0] if not any( [first.endswith(e) for e in noen] ) else ''
        er = '' if rnd.getrandbits(3) < 1 else 'er'
        last = rnd.sample(part2, 1)[0]
        print( first + (en if bool(rnd.getrandbits(1)) else '') + last + er )


if __name__ == '__main__':
    progname = os.path.splitext(os.path.basename( __file__ ))[0]
    vstring = (' v0.22\nWritten by christoph.bischko@gmail.com\n'
              '(Mo 21. Mär 17:21:18 CET 2016) on Bischko-PC')
    args = docopt(__doc__ % locals(), version=progname+vstring)
    logging.basicConfig(
        level   = logging.ERROR - int(args['--verbose'])*10,
        format  = '[%(levelname)-7.7s] (%(asctime)s '
                  '%(filename)s:%(lineno)s) %(message)s',
        datefmt = '%y%m%d %H:%M'   #, stream=, mode=, filename=
    )

    do_stuff(args)

