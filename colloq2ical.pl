#!/usr/bin/perl -w
# Converts a specially formated text file (list of events) into an .ics
# (iCalendar) file for import into Google-Calendar or other calendar
# applications.
#
# Parse Mails sent out about dates and location of the physics colloquium
# talks.
#
# Website:
#   https://bitbucket.org/jacgb/uibk-scripts/src/master/

use strict; use warnings; # For larger files use File::Slurp or Tie::File

use Date::ICal;
use Data::ICal;
use Data::ICal::Entry::Event;
use Data::ICal::Entry::FreeBusy;
use Data::ICal::Entry::Alarm::Display;

# Read file into one string
open my $DATEFILE, "<", $ARGV[0] or die "Couldn't open file: $!";
my $fstring = join("", <$DATEFILE>);
close $DATEFILE or warn "Couldn't close file: $!'";

# Prepare calendar
my $calendar = Data::ICal->new();
$calendar->add_properties( method=>"PUBLISH",);

# Add events depending on what was found in file
my @events = split("\n\n", $fstring);
foreach my $eventstring (@events) {
    $eventstring =~ s/\h+/ /g;
    $eventstring =~ s/(\d+\.\d+\.\d+)//;
    my $datestring = $1;
    (my $day, my $month, my $year) = split(/\./, $datestring);
    my $title = ( split /\n/, $eventstring )[1];

    my $start = Date::ICal->new( year=>"20$year", month=>$month, day=>$day, hour=>17, min=>15 )->ical;
    my $end   = Date::ICal->new( year=>"20$year", month=>$month, day=>$day, hour=>18, min=>15 )->ical;

    my $event = Data::ICal::Entry::Event->new();
    $event->add_properties(
        summary     => "Kolloquium: $title",
        description => $eventstring,
        dtstart     => $start,
        dtend       => $end,
        dtstamp     => Date::ICal->new( epoch => time )->ical,
    );
    $calendar->add_entry($event);
}

print $calendar->as_string;
