#!/usr/bin/python
# coding: UTF-8, break: linux, indent: 4 spaces, lang: python/eng
"""
Convert points to grades given the upper bounds for these grades in order.

Usage:
    {progname} --help | --version
    {progname} [options] [-v...] -u=<upper>... ARGS...

Options:
    -v --verbose   Specify (multiply) to increase output
    -u=<upper>     Upper bound for a grade (define mulitple times)
    -s             Output corresponding grades separated by newline
                   characters without points.

Example:
    A grading system from 1 to 5, where a 1 is given from <13 to <=16 points.

      $ {progname} -u 16 -u 13 -u 10 -u 8 9 14  #synonoumous to line below
      $ {progname} -u "16 13 10 8" 9 14
       9.00 pts ---> grade 3.00
      14.00 pts ---> grade 1.17
"""

#=======================================================================

from __future__ import division, print_function, unicode_literals
import os
from docopt import docopt

#=======================================================================

def clst(val, lst):
    '''Get index and value of the element in `lst` closest to `val`.'''
    val = min(lst, key=lambda x:abs(x-val))
    return lst.index(val), val

def upper2grades(pnts, upper):
    '''Get fractional grade from point upper limits.'''
    mid = []; low = []
    for i, elem in enumerate(upper):
        try:
            low.append( upper[i+1] )
        except:
            low.append(0.0)
        mid.append( ( elem + low[i] )*0.5 )
    ret = []
    for elem in pnts:
        idx, val = clst(elem, mid)
        diff = elem-val
        norm = low if diff>0 else upper
        ret.append(idx + 1 - 0.5*diff/abs(val-norm[idx]))
    return ret

def do_stuff(args):
    if len(args['-u']) == 1:
        args['-u'] = args['-u'][0].split()

    grades = upper2grades(
        [float(x) for x in args['ARGS']],
        [float(x) for x in args['-u']]
    )
    if args['-s']:
        for elem in grades: print( "{:.2f}".format(elem) )
    else:
        for idx, elem in enumerate(grades):
            print( "{:5.2f} pts ---> grade {:.2f}".format(
                float(args['ARGS'][idx]),
                elem
            ) )

if __name__ == '__main__':
    progname = os.path.splitext(os.path.basename( __file__ ))[0]
    vstring = (' v0.1\nWritten by christoph.bischko@gmail.com\n'
               '(Di 5. Jul 15:15:45 CEST 2016) on Bischko-PC'    )
    args = docopt(__doc__.format(**locals()), version=progname+vstring)

    do_stuff(args)
