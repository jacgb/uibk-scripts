#!/usr/bin/python
# coding: UTF-8, break: linux, indent: 4 spaces, lang: python/eng
"""
Retrieve the closest course dates from the university homepage and print
OLAT compatible selectors for turn-in of assignments during the course
times.

Copy paste these in the respective fields "Assignment", "Dropbox" and
"Sample Solution" of an OLAT "Task" element (under "Access") and students
will only be able to view the task and submitt their suolutions during
the resp. course.

See: http://lms.uibk.ac.at

Website:
    https://bitbucket.org/jacgb/uibk-scripts/src/master/

Usage:
    %(progname)s --help | --version
    %(progname)s [options] [-v...]

Options:
    -v --verbose        Specify (multiply) to increase output verosity
    --lvid=<str>        Identifier of the course [default: 706021]
    --semester=<str>    Identifier of the semester in which the course
                        is taking place [default: %(defaultsem)s]
    --groups=<int>      Total number of Groups [default: 4]
    --days=<int>        Minimum number of days the course date should be
                        in the future [default: 7]
    --offset=<int>      Offset for the course start and end in minutes
                        [default: 15]
"""

#=======================================================================

from __future__ import division, print_function, unicode_literals
import os, urllib2, time, datetime, pytz, os, logging
from logging import info, debug, error, warning as warn
from docopt import docopt
from icalendar import Calendar
from datetime import tzinfo, timedelta
from dateutil import tz

#=======================================================================

if __name__ == '__main__':
    progname = os.path.splitext(os.path.basename( __file__ ))[0]
    vstring = (' v0.3\nWritten by jan.bischko@uibk.ac.at\n'
              '(Di 15. Mär 13:39:29 CET 2016) on Bischko-PC')
    # Semester ids: 15W -> winter 2015/2016
    #               16S -> summer 2016
    y, m, d, h, mi, s, wd, yd, dst = time.localtime(time.time())
    y -= 2000
    defaultsem = str(y-1)+'W' if m<3 else str(y)+'W' if m>9 else str(y)+'S'
    args = docopt(__doc__ % locals(), version=progname+vstring)
    logging.basicConfig(
        level   = logging.ERROR - int(args['--verbose'])*10,
        format  = '[%(levelname)-7.7s] (%(asctime)s '
                  '%(filename)s:%(lineno)s) %(message)s',
        datefmt = '%y%m%d %H:%M'   #, stream=, mode=, filename=
    )
    now = pytz.utc.localize( datetime.datetime.now() )
    qtr = timedelta(minutes=int(args['--offset']))

    assignment, dropbox, solution = '', '', ''
    for group in range(   0, int(args['--groups'])   ):
        url = ("https://orawww.uibk.ac.at/public/"
               "lfuonline_lv.details_ical?lvnr_id_in={}"
               "&sem_id_in={}&gruppen_id_in={}").format(
                    args['--lvid'], args['--semester'], group
                )
        info("Retreiving '%s'...", url)
        response = urllib2.urlopen( urllib2.Request(url) )
        data = response.read()

        cal = Calendar.from_ical(data)

        tmpold = now + timedelta(days=1e6) # No years? really?
        latest = now
        for event in cal.walk('vevent'):
            if event.name == "VEVENT":
                # I freakin' hate timezones, next time use Arrow
                tmpstart = event.get('dtstart').dt#.replace(tzinfo=None)
                tmpend = event.get('dtend').dt#.replace(tzinfo=None)
                summary = event.get('summary').to_ical().decode('utf-8')
                descr = event.get('description').to_ical().decode('utf-8')
                local = event.get('location').to_ical().decode('utf-8')
                debug("\t%s - %s", tmpstart.ctime(), summary)

                if now + timedelta(days=int(args['--days'])) > tmpstart:
                    continue

                if tmpstart < tmpold:
                    start, end, grp = tmpstart+qtr, tmpend+qtr, group

                tmpold = tmpstart

        start = start.astimezone(tz.tzlocal()) # ...sooo much!
        end = end.astimezone(tz.tzlocal())
        latest = end if latest < end else latest

        assignment += \
            '((inLearningGroup(\"{}{} Gruppe {}\")) & (now > date(\"{}\"))) |\n'.format(
                '20'+args['--semester'], args['--lvid'], grp,
                start.strftime("%d.%m.%Y %H:%M")
            )
        dropbox += \
            '((inLearningGroup(\"{}{} Gruppe {}\")) & (now > date(\"{}\")) & (now < date(\"{}\"))) |\n'.format(
                '20'+args['--semester'], args['--lvid'], grp,
                start.strftime("%d.%m.%Y %H:%M"),
                end.strftime("%d.%m.%Y %H:%M")
            )
        solution = \
            '((inLearningGroup("{}{} Gruppe {}")) & (now > date("{}"))) |\n'.format(
                '20'+args['--semester'], args['--lvid'], grp,
                end.strftime("%d.%m.%Y %H:%M")
            )

    coaches = '( isCourseCoach(0) | isCourseAdministrator(0) )\n'
    assignment = 'ASSIGNMENT:\n' + assignment + coaches
    dropbox    = 'DROP BOX:\n'   + dropbox + coaches
    solution   = 'SOLUTION:\n'   + solution + coaches

    print( assignment )
    print( dropbox    )
    print( solution   )
