Miscellaneous UIBK-Scripts
===============

This is a collections of scripts used to interface with various systems
of the [University of Innsbruck][uibk], e.g. the [course catalogue][courses], [OLAT][olat],
or varius automated E-Mails.

 - [propra unpack](./unarchive-olat.sh) - Unpack a task submissions archive from the archive tool on OLAT
 - [grades](./grades.py) - Convert points to fractional grades, e.g. 13 pts ---> 1.3 ("very good" with a tendency to just "good")
 - [olat users to shell array](./olatuser2array.py) - Convert an excel sheet (`.xls`) containing the member list of an OLAT course to a shell array
 - [next course](./nextcourse.py) - Get the next date of the given course from the `.ical` files on the university website and generate rules for task submission visibility for use with OLAT.
 - [colloq to ical](./colloq2ical.pl) - Convert mail about the physics colloquium to an `.ical` file
 - [tirolian last name](./tirolianlastname.py) - Generate more or less reallistiaclly sounding tyrolian last names

See comments in the individual files for more details.

[uibk]: https://www.uibk.ac.at
[olat]: https://lms.uibk.ac.at
[courses]: https://orawww.uibk.ac.at/public/lfuonline_lv.home

Hosted on https://bitbucket.org/jacgb/uibk-scripts/src/master/
