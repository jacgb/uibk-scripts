#!/usr/bin/python
# coding: UTF-8, break: linux, indent: 4 spaces, lang: python/eng
"""
List participants with their user-id contained in an OLAT xls participant list
from "Members management" -> "Members".

Website:
    https://bitbucket.org/jacgb/uibk-scripts/src/master/

Usage:
    %(progname)s --help | --version
    %(progname)s [options] [-v...] [FILE...]

Options:
    -v --verbose   Specify (multiply) to increase output
"""

#=======================================================================

from __future__ import division, print_function#, unicode_literals
from logging import info, debug, error, warning as warn
from docopt import docopt
import matplotlib.pyplot as plt
import os, glob, logging
import pyExcelerator as pxls

#=======================================================================

def get_col_by_kw(kw, dct):
    for k, v in dct.iteritems():
        if kw in v.lower() and k[0] == 0:
            return k[1]
    return None;

def do_stuff(args):
    if not args['FILE']: args['FILE'] = glob.glob('*.xls')

    count = 0
    for file in args['FILE']:
        for sheet_name, values in pxls.parse_xls(file):
            vsorted = sorted(values.keys())
            usercol   = get_col_by_kw('user' , values)
            givencol  = get_col_by_kw('first', values)
            familycol = get_col_by_kw('last' , values)

            if None in [usercol, givencol, familycol]:
                warn("Couldn't get colums from '%s'!", file)
                break;

            debug( "usercol: %i, givencol: %i, familycol: %i",
                          usercol,       givencol,     familycol )

            user, given, family = (
                ['']*len(vsorted), ['']*len(vsorted), ['']*len(vsorted) )

            for row_idx, col_idx in vsorted:
                val = values[(row_idx, col_idx)]
                if   col_idx == usercol:
                    user[row_idx] = val
                    count += 1
                elif col_idx == givencol:
                    given[row_idx] = val
                    count += 1
                elif col_idx == familycol:
                    family[row_idx] = val
                    count += 1

            count /= 3
            if not (count).is_integer():
                warn("Wrong participant count in '%s'!", file)
                count = int(count)
                break;
            count = int(count)

            for item in zip(user, given, family)[1:count]:
                debug( item )
                print(
                    '[{}]="{}_{}"'.format(
                        item[0].encode('utf8'),
                        item[2].encode('utf8'),
                        item[1].encode('utf8').replace(' ', '.')
                    )
                )

    print(   '# Total participants: '+ str(count)   )


if __name__ == '__main__':
    progname = os.path.splitext(os.path.basename( __file__ ))[0]
    vstring = (' v0.1\nWritten by christoph.bischko@gmail.com\n'
              '(Mo 21. Mär 14:56:18 CET 2016) on Bischko-PC')
    args = docopt(__doc__ % locals(), version=progname+vstring)
    logging.basicConfig(
        level   = logging.ERROR - int(args['--verbose'])*10,
        format  = '[%(levelname)-7.7s] (%(asctime)s '
                  '%(filename)s:%(lineno)s) %(message)s',
        datefmt = '%y%m%d %H:%M'   #, stream=, mode=, filename=
    )

    do_stuff(args)
