#!/bin/bash
###############################################################################
# Unpacks and formats OLAT archive tool output.
#
# If you cannot use `olatuser2array.py`, you will need to edit the participants
# array yourself to reflect your course data.
#
# Website:
#    https://bitbucket.org/jacgb/uibk-scripts/src/master/
#
# Usage:
#   ./unarchive-olat.sh SOLUTION-FILE USER-FILE
#
##############################################################################

scdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Sanity checks
[ ${#@} != 2 ] && { echo "Usage: $0 ARCHIVE-FILE PARTICIPANTS-FILE" 1>&2; exit 1; }
hash indent || { echo "The indent program was not found on your system! Trying to install it:" 1>&2; sudo apt-get install indent || exit 1; }
hash unzip || { echo "The unzip program was not found on your system! Trying to install it:" 1>&2; sudo apt-get install unzip || exit 1; }

participantstr=$("$scdir"/olatuser2array.py "$2" || echo "")
[ -z "$participantstr" ] && { echo "Error: Cannot read '$2' or an OLAT course member list!" 1>&2; exit 1; }

{ particode=$(</dev/stdin); } <<- EndHereDoc
    declare -A participants=(
        $participantstr
    )
EndHereDoc
eval "$particode"
# names by cs-numbers from OLAT member management, e.g.
# declare -A participants=(
# [csat3723]="Schwarzwalz_Florian"
# [csas8516]="Reschenegg_Peter"
# [csas7806]="Mooswalder_Konrad.Alexander"
# [csat3227]="Langhauser_Markus"
# [csaq2040]="Mauwalzer_David"
# )

rawrname=${1%.*}
echo "rname: $rname"

rname=$(unzip "$rawrname.zip" || echo "error")
[ "$rname" == "error" ] && { echo "Error: Cannot unzip '$rawrname', does it exist?" 1>&2; exit 1; }
# get root directory within the archive
rname=$(echo "$rname" | grep -m1 "creating: ")
rname=${rname##*creating: }
rname=${rname%%/*}

olddir=$(pwd)
cd "$rname"
# Unzip and move all *.c files to the appropriate dir, delete the rest
for guy in "dropboxes/"*; do
    # Get cs-number
    handle=$(echo "$guy" | grep -oP "cs[a-z]{2,2}[0-9]{4,4}")
    [ -z "$handle" ] && echo "Participant '$guy' doesn't have a valid cs-number!" 1>&2 && continue
    # Look cs-number up in participant list and convert special chars
    guyname=$(echo "${participants[$handle]}" | LC_ALL=de_DE.UTF-8 iconv -t ASCII//TRANSLIT -f UTF-8)
    [ -z "$guyname" ] && echo "Participant '$handle' not in list!" 1>&2 && continue
    echo "dropbox: $guy --> extracted cs-handle: $handle; extracted name: $guyname"
    # Replace cs-number by participant's name
    mv "$guy" "$guyname"
    # Extract solutions and treat source file with indent
    find "$guyname" -type f -iregex ".*\.\(tar\|tgz\|tar.gz\)" -exec tar --keep-newer-files -C "$guyname" -xvf {} \; -and -exec rm {} \;
    find "$guyname" -iname "*.zip" -exec unzip -d "$guyname" {} \; -and -exec rm {} \;
    # Next line only appies to solutions of programming exercies in C-language
    find "$guyname" -iregex ".*\.\(txt\|c\)" -type f -exec indent -kr {} \; -and -exec cp --backup=t {} "$guyname/" \;
done
rm -r "dropboxes" "taskfolders"
cd "$olddir"
